const express = require('express');
const Debt = require('../models/Debt');
const router = express.Router();

// Get all debts
router.get('/', async (req, res) => {
  try {
    const debts = await Debt.find();
    res.json(debts);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Create a new debt
router.post('/', async (req, res) => {
  const debt = new Debt(req.body);

  try {
    const newDebt = await debt.save();
    res.status(201).json(newDebt);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Get a specific debt by ID
router.get('/:id', getDebtById, (req, res) => {
  res.json(res.debt);
});

// Update a specific debt by ID
router.put('/:id', getDebtById, async (req, res) => {
  try {
    Object.assign(res.debt, req.body);
    const updatedDebt = await res.debt.save();
    res.json(updatedDebt);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Delete a specific debt by ID
router.delete('/:id', getDebtById, async (req, res) => {
  try {
    await res.debt.remove();
    res.json({ message: 'Debt deleted' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Middleware to find a debt by ID
async function getDebtById(req, res, next) {
  let debt;
  try {
    debt = await Debt.findById(req.params.id);
    if (!debt) {
      return res.status(404).json({ message: 'Debt not found' });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }

  res.debt = debt;
  next();
}

module.exports = router;