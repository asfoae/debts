const mongoose = require('mongoose');

const debtSchema = new mongoose.Schema({
  creditor: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  initialBalance: {
    type: Number,
    required: true,
  },
  balanceLeft: {
    type: Number,
    required: true,
  },
  currency: {
    type: String,
    required: true,
  },
  interestRate: {
    type: Number,
    required: true,
  },
  startDate: {
    type: Date,
    required: true,
  },
  duration: {
    value: {
      type: Number,
      required: true,
    },
    unit: {
      type: String,
      enum: ['week', 'month', 'year'],
      required: true,
    },
  },
  paymentFrequency: {
    value: {
      type: Number,
      required: true,
    },
    unit: {
      type: String,
      enum: ['week', 'month', 'year'],
      required: true,
    },
  },
  isRevolving: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model('Debt', debtSchema);
